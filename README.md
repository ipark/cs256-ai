# CS256 - Practical Computer Vision using Neural Network (Deep Learning)
1  Introduction

2  Linear Classification

3  Optimization

3  Backpropagation

4  Convolutional Neural Networks

5  Introduction to MXNet (AWS).......[Milestone1 report is due on 9-13]

6  Literature Survey Presentations 

7  Hardware and Software for Deep Learning

8  Training a Network 

9  Architectures: AlexNet, VGG, GoogleNet

10 Architectures: ResNet, MobileNet, HRNet

11 Recurrent Networks

12 Use-case Study (Nvidia).......[Milestone2 report is due on 10-14]

13 Checkpoint A presentations 

14 Generative Models

15 Reinforcement Learning

16 Detection: R-CNN, Fast and Faster R-CNN

17 Detection: YOLO and SSD.......[Milestone3 report is due on 11-4]

18 Segmentation

19 Pose Estimation

20 Final Project presentations 
